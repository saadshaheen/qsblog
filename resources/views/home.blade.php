@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                   <a href="{{url('/posts/create')}}" class="btn btn-primary">Add posts</a>
                   <h3 class="text-uppercase">your blog posts</h3>
                    @if(count($posts)>0)
                        <table class="table table-striped">
                                   <tr>
                                       <th> title </th>
                                        <th> </th>
                                   </tr>   
                           @foreach($posts as $post)
                                  <tr>
                                    <th>{{$post->title}} </th>
                                     <th> 
                                          <a href="{{url('posts/'.$post->id.'/edit')}}">
                                               Edit
                                          </a> 
                                     </th>
                                  </tr>
                           @endforeach 
                       </table> 
                   @else
                     <p> there is no post availabe</p>
                   @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
