@extends('layouts.app')


@section('content')
  <h1> Create the Post</h1>
  {!! Form::open(['action' => ['PostsController@update',$post->id] , 'method'=>'POST' , 'enctype' => 'multipart/form-data']) !!}
    
    <div>
    	{{form::label('title', 'Title')}}
    	{{form::text('title' ,$post->title,['class'=>'form-control ', 'placeholder' =>'Title'])}}


    </div>
    <div>
    	{{form::label('body', 'Body')}}
    	{{form::textarea('body' ,$post->body ,['class'=>'form-control', 'placeholder' =>'Body'])}}


    </div>
      {{form::hidden('_method','PUT')}}
      <div class="form-group">
        {{form::file('cover_image')}} 
    </div>

    {{form::submit('Submit',['class'=> 'btn btn-primary m-3'])}}

{!! Form::close() !!}


@endsection