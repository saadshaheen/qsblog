@extends('layouts.app')


@section('content')
<a href="{{url('posts')}}" class="btn btn-primary">Go Back</a>
<h1>{{$post->title}}</h1>

      <div class="row">
      	 <div class="col-md-12"> 
      	 	<img src="../storage/cover_images/{{ $post->cover_image }}">
      	 </div>
      	
      </div>
<p>{{$post->body}}</p>
  


<hr>

  <small>written on {{$post->created_at}}</small>
  <hr>

    @if(!Auth::guest())
      @if(Auth::User()->id == $post->user_id)
		<a href="{{url('posts/'.$post->id.'/edit')}}"  class="btn btn-default btn-secondary">Edit</a>
		{!! Form::open(['action' => ['PostsController@destroy',$post->id] , 'method'=>'POST', 'class' => 'pull-right']) !!}
		{{form::hidden('_method','DELETE')}}
		{{form::submit('Delete',['class'=> 'btnd btn-danger mt-3'])}}
		{!! Form::close() !!}
	  @endif
  @endif
@endsection