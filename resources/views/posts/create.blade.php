@extends('layouts.app')


@section('content')
  <h1> Create the Post</h1>
  {!! Form::open(['action' => 'PostsController@store' , 'method'=>'POST' , 'enctype' => 'multipart/form-data']) !!}
    
    <div>
    	{{form::label('title', 'Title')}}
    	{{form::text('title' ,'' ,['class'=>'form-control ', 'placeholder' =>'Title'])}}


    </div>
    <div>
    	{{form::label('body', 'Body')}}
    	{{form::textarea('body' ,'' ,['class'=>'form-control', 'placeholder' =>'Body'])}}
    </div>
    <div class="form-group">
        {{form::file('cover_image')}} 
    </div>
    {{form::submit('Submit',['class'=> 'btn btn-primary m-3'])}}

{!! Form::close() !!}


@endsection