@extends('layouts.app')


@section('content')

<h1>Posts</h1>
 @if(count($posts) > 0)
   <div class="card">
       <ul>
   @foreach($posts as $post)
    
      	 


          <div class="row ">
           
                  <div class="col-md-4">
                      <li class="mt-5"> <img style="height: 120%; width: 100%" src="storage/cover_images/{{$post->cover_image}}" ></li>
                  </div>  
                  <div class="col-md-8">
                         <li class="list-group-item">
                         <h1><a href="{{url('/posts/'.$post->id) }}">{{$post->title}}</a></h1> 

                        <small>written on{{$post->created_at}}</small>  
                      </li>
                </div>

          </div>
       

   @endforeach
      </ul>
   </div>
 @else

@endif

@endsection